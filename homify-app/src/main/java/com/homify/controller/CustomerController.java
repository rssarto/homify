package com.homify.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.homify.enums.ContractTypeEnum;
import com.homify.model.Contract;
import com.homify.model.Customer;
import com.homify.service.CustomerService;
import com.homify.service.impl.CustomerServiceImpl;

@RestController
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	@Qualifier("customerValidator")
	private Validator customerValidator;
	
	@Autowired
	@Qualifier("contractValidator")
	private Validator contractValidator;
	
	@InitBinder(value="customer")
	public void initCustomerBinder(WebDataBinder binder) {
		binder.setValidator(customerValidator);
	}
	
	@InitBinder(value="contract")
	public void initContractBinder(WebDataBinder binder) {
		binder.setValidator(contractValidator);
	}
	
	/***
	 * Add a new customer
	 * As we're using a sychronized context in CustomerServiceImpl performance can decrease in a multithread context. 
	 * @param customer
	 * @param customerId
	 * @return boolean
	 */
	@RequestMapping(value="/customer/{id}", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ModelMap> addCustomer(@RequestBody @Valid Customer customer, @PathVariable(name="id") int customerId, BindingResult binding)throws Exception{
		ResponseEntity<ModelMap> responseEntity = null;
		
		if( !binding.hasErrors() ){
			customerService.addCustomer(customer);
			responseEntity = new ResponseEntity<>(new ModelMap("result", true), HttpStatus.OK);
		}
		
		return responseEntity;
	}
	
	/***
	 * Add a new contract to an existing customer
	 * As we're using a sychronized context in CustomerServiceImpl performance can decrease in a multithread context. 
	 * @param contract
	 * @param contractId
	 * @return boolean
	 */
	@RequestMapping(value="/contract/{id}", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ModelMap> addContract(@RequestBody @Valid Contract contract, @PathVariable(name="id") int contractId, BindingResult binding)throws Exception{
		ResponseEntity<ModelMap> responseEntity = null;
		
		if( !binding.hasErrors() ){
			Customer customer = customerService.getCustomer(contract.getCustomerId());
			if( customer != null ){
				List<Contract> contracts = customer.getContracts();
				if( contracts ==  null ){
					contracts = new ArrayList<>();
				}
				contracts.add(contract);
				customer.setContracts(contracts);
				customerService.updateCustomer(customer);
				responseEntity = new ResponseEntity<>(new ModelMap("result", true), HttpStatus.OK);
			}
		}
		
		return responseEntity;
	}
	
	/***
	 * Retrieves all the information about an existing customer, including their contracts information
	 * @param customerId
	 * @return Customer
	 */
	@RequestMapping(value="/customer/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> getCustomer(@PathVariable(name="id") int customerId){
		ResponseEntity<Customer> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		Customer customer = customerService.getCustomer(customerId);
		if( customer != null ){
			responseEntity = new ResponseEntity<Customer>(customer, HttpStatus.OK);
		}
		return responseEntity;
	}
	
	/***
	 * Retrieves the sum of revenues of all contracts from an existing customer
	 * Accordingly to specification the request needed to be mapped like 'GET /customerservice/contract/$customer_id'. 
	 * But to avoid conflict with 'GET /customerservice/contract/$type' the request was mapped like below. 
	 * @param customerId
	 * @return double
	 */
	@RequestMapping(value="/contract/id/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ModelMap> contractRevenuesByCustomer(@PathVariable(name="id") int customerId){
		ResponseEntity<ModelMap> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		Customer customer = customerService.getCustomer(customerId);
		if( customer != null ){
			responseEntity = new ResponseEntity<ModelMap>(new ModelMap("sum", CustomerService.sumRevenues(customer.getContracts())), HttpStatus.OK);
		}
		return responseEntity;
	}
	
	/***
	 * Retrieves the sum of revenues of all contracts of a specific type
	 * Accordingly to specification the request needed to be mapped like 'GET /customerservice/contract/$type'. 
	 * But to avoid conflict with 'GET /customerservice/contract/$customer_id' the request was mapped like below. 
	 * @param type
	 * @return double
	 */
	@RequestMapping(value="/contract/type/{type}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ModelMap> contractRevenuesByType(@PathVariable(name="type") ContractTypeEnum type){
		return new ResponseEntity<>(new ModelMap("sum", CustomerService.sumRevenues(CustomerServiceImpl.getContractsByType(type))), HttpStatus.OK);
	}
}
