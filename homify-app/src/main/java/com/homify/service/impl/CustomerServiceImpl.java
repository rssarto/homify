package com.homify.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import com.homify.enums.ContractTypeEnum;
import com.homify.model.Contract;
import com.homify.model.Customer;
import com.homify.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	private static final Map<Integer, Customer> CUSTOMER_RECORDS = new HashMap<>();

	/***
	 * Add a new customer to CUSTOMER_RECORDS map.
	 * @param customer
	 */
	@Override
	public void addCustomer(Customer customer) {
		synchronized(CustomerServiceImpl.class){
			if( this.getCustomer(customer.getId()) == null ){
				CUSTOMER_RECORDS.put(customer.getId(), customer);
			}
		}
	}

	/***
	 * Update customer in CUSTOMER_RECORDS map.
	 * @param customer
	 */
	@Override
	public void updateCustomer(Customer customer) {
		synchronized(CustomerServiceImpl.class){
			if( CUSTOMER_RECORDS.containsKey(customer.getId()) ){
				CUSTOMER_RECORDS.replace(customer.getId(), customer);
			}else{
				CUSTOMER_RECORDS.put(customer.getId(), customer);
			}
		}
	}

	/***
	 * Get customer from CUSTOMER_RECORDS map.
	 * @param id
	 * @return Customer
	 */
	@Override
	public Customer getCustomer(int id) {
		if( CUSTOMER_RECORDS.containsKey(id) ){
			return CUSTOMER_RECORDS.get(id);
		}
		return null;
	}

	/***
	 * Filters all contracts that have the same type as the argument.
	 * @param type
	 * @return List<Contract>
	 */
	public static List<Contract> getContractsByType(ContractTypeEnum type) {
		return CustomerService.filterContracts(getAllContracts(), (contract) -> {return contract.getType() == type;} );
	}
	
	public static List<Contract> getAllContracts(){
		List<Contract> contracts = null;
		if( CUSTOMER_RECORDS.size() > 0 ){
			for( Entry<Integer, Customer> entry : CUSTOMER_RECORDS.entrySet() ){
				Customer customer = entry.getValue();
				
				if( customer.getContracts() != null ){
					
					if(contracts == null){
						contracts = new ArrayList<>();
					}
					
					contracts.addAll(customer.getContracts());
				}
			}
		}
		return contracts;
	}
}
