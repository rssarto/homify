package com.homify.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.homify.model.Contract;
import com.homify.model.Customer;

public interface CustomerService {

	abstract void addCustomer(Customer customer);
	abstract void updateCustomer(Customer customer);
	abstract Customer getCustomer(int id);
	
	/***
	 * Generates a filtered list applying the contractTest argument on each contract from the argument list. 
	 * @param contracts
	 * @param contractTest
	 * @return List<Contract>
	 */
	public static List<Contract> filterContracts(List<Contract> contracts, Predicate<Contract> contractTest){
		List<Contract> filteredContracts = new ArrayList<>();
		if( contracts != null ){
			for( Contract contract : contracts ){
				if( contractTest.test(contract) ){
					filteredContracts.add(contract);
				}
			}
		}
		return filteredContracts;
	}
	
	/***
	 * Sums the revenue of each contract inside the argument list.
	 * @param contracts
	 * @return double
	 */
	public static double sumRevenues(List<Contract> contracts){
		double sum = 0.0;
		if( contracts != null ){
			for( Contract contract : contracts ){
				System.out.println(contract);
				sum += contract.getRevenue();
			}
		}
		return sum;
	}
}
