package com.homify.model;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	private int id;
	private String fullName;
	private String email;
	private List<Contract> contracts;
	
	public Customer(){}
	
	public Customer(int id, String fullName, String email, List<Contract> contracts) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.email = email;
		if( contracts != null )
			this.contracts = new ArrayList<>(contracts);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public List<Contract> getContracts() {
		if( this.contracts != null ){
			return new ArrayList<>(contracts);	
		}
		return null;
	}

	public void setContracts(List<Contract> contracts) {
		if( contracts != null && contracts.size() > 0 ){
			this.contracts = new ArrayList<>(contracts);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if( obj == null )
			return false;
		
		if( !(obj instanceof Customer) )
			return false;
		
		Customer other = (Customer) obj;
		
		return this.id == other.id;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", fullName=" + fullName + ", email=" + email + ", contracts=" + contracts + "]";
	}
}
