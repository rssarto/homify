package com.homify.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.homify.enums.ContractTypeEnum;

public class Contract {
	
	private int id;
	private int customerId;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date start;
	private ContractTypeEnum type;
	private double revenue;
	
	public Contract(){}
	
	public Contract(int id, int customer, Date start, ContractTypeEnum type, double revenue) {
		super();
		this.id = id;
		this.customerId = customer;
		this.start = start;
		this.type = type;
		this.revenue = revenue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public ContractTypeEnum getType() {
		return type;
	}

	public void setType(ContractTypeEnum type) {
		this.type = type;
	}

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	@Override
	public String toString() {
		return "Contract [id=" + id + ", customerId=" + customerId + ", start=" + start + ", type=" + type
				+ ", revenue=" + revenue + "]";
	}
}
