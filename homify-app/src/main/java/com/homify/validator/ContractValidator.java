package com.homify.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.homify.model.Contract;
import com.homify.model.Customer;
import com.homify.service.CustomerService;

@Component
public class ContractValidator implements Validator {
	
	@Autowired
	private CustomerService customerService;

	@Override
	public boolean supports(Class<?> classParam) {
		return Contract.class.equals(classParam);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		Contract contract = (Contract) obj;
		
		if( !(contract.getCustomerId() > 0) ){
			errors.rejectValue("customerId", "required", null, "Customer Id is required.");
		}else{
			if( customerService.getCustomer(contract.getCustomerId()) == null ){
				errors.rejectValue("customerId", "notFound", null, "No customer found for id " + contract.getCustomerId());
			}
		}
		
		if( !(contract.getId() > 0) ){
			errors.rejectValue("id", "required", null, "Id is required.");
		}else{
			if( contract.getCustomerId() > 0 ){
				Customer customer = customerService.getCustomer(contract.getCustomerId());
				if( customer != null ){
					if( CustomerService.filterContracts(customer.getContracts(), (contractParam) -> {return contractParam.getId() == contract.getId();} ).size() > 0 ){
						errors.rejectValue("id", "duplicatedContract", null, "Contract already exist for customer");
					}
				}
			}
		}
		
		if( !(contract.getId() > 0) ){
			errors.rejectValue("revenue", "required", null, "Revenue is required.");
		}
		
		if( contract.getStart() == null ){
			errors.rejectValue("start", "required", null, "Start date is required.");
		}
		
		if( contract.getType() == null ){
			errors.rejectValue("type", "required", null, "Contract type is required.");
		}
		
		if( !(contract.getRevenue() > 0) ){
			errors.rejectValue("revenue", "required", null, "Revenue is required.");
		}
	}

}
