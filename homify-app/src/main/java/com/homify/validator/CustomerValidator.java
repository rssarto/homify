package com.homify.validator;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.homify.model.Customer;
import com.homify.service.CustomerService;

@Component
public class CustomerValidator implements Validator {
	
	@Autowired
	private CustomerService customerService;

	@Override
	public boolean supports(Class<?> paramClass) {
		return Customer.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		Customer customer = (Customer)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fullName", "required", "Full name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required", "Email is required.");
		
		if( !(customer.getId() > 0) ){
			errors.rejectValue("id", "required", null, "Id is required.");
		}else{
			if( customerService.getCustomer(customer.getId()) != null ){
				errors.rejectValue("id", "alreadyExist", null, "Id already exists.");
			}
		}
		
		if( !customer.getEmail().trim().equals("") ){
			EmailValidator emailValidator = new EmailValidator();
			if( !emailValidator.isValid(customer.getEmail(), null) ){
				errors.rejectValue("email", "invalid", null, "Email is invalid.");
			}
		}
	}
}
