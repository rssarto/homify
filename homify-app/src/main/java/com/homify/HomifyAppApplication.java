package com.homify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication()
@ComponentScan(basePackages="com.homify")
public class HomifyAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomifyAppApplication.class, args);
	}
}
