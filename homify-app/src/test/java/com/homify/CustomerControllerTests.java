package com.homify;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.homify.controller.CustomerController;
import com.homify.enums.ContractTypeEnum;
import com.homify.model.Contract;
import com.homify.model.Customer;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTests {
	
	private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

	@Autowired
	private MockMvc mvc;
	
	@Test
	public void addNewCustomers() throws Exception{
		Customer customer = new Customer(1, "RICARDO SOARES SARTO", "ricardo@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		
		customer = new Customer(2, "ALEXANDRE SOARES SARTO", "alexandre@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
	}
	
	@Test
	public void addDuplicatedCustomer() throws Exception{
		Customer customer = new Customer(3, "ALAIDE SOARES SARTO", "alaide@test.com", null); 
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		
		customer = new Customer(3, "MARCOS SOARES SARTO", "marcos@test.com", null);
		this.addCustomer(customer, HttpStatus.BAD_REQUEST);
		this.getCustomer(customer.getId(), HttpStatus.OK);
	}
	
	@Test
	public void addCustomerNoId() throws Exception{
		Customer customer = new Customer(0, "MARCOS SOARES SARTO", "marcos@test.com", null);
		this.addCustomer(customer, HttpStatus.BAD_REQUEST);
		this.getCustomer(customer.getId(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void addCustomerNoName() throws Exception{
		Customer customer = new Customer(4, "", "anonymous@test.com", null);
		this.addCustomer(customer, HttpStatus.BAD_REQUEST);
		this.getCustomer(customer.getId(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void addCustomerNoEmail() throws Exception{
		Customer customer = new Customer(5, "ANTONIO CLAUDIO SARTO", "", null);
		this.addCustomer(customer, HttpStatus.BAD_REQUEST);
		this.getCustomer(customer.getId(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void addCustomerInvalidEmail() throws Exception{
		Customer customer = new Customer(6, "ANTONIO CLAUDIO SARTO", "antonio.com", null);
		this.addCustomer(customer, HttpStatus.BAD_REQUEST);
		this.getCustomer(customer.getId(), HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void addContractToCustomer() throws Exception{
		Customer customer = new Customer(7, "ANTONIO CLAUDIO SARTO", "antonio@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 7, new Date(), ContractTypeEnum.BASIC, 1500.50), HttpStatus.OK);
	}
	
	@Test
	public void addContractToInvalidCustomer() throws Exception{
		this.addContract(new Contract(1, 10000, new Date(), ContractTypeEnum.BASIC, 1500.50), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addDuplicatedContract() throws Exception{
		Customer customer = new Customer(8, "VIVIANE SARTO", "viviane@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 8, new Date(), ContractTypeEnum.COMPACT, 150.50), HttpStatus.OK);
		this.addContract(new Contract(1, 8, new Date(), ContractTypeEnum.FULL, 2550.50), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addContractNoId()throws Exception{
		Customer customer = new Customer(9, "RAFAELA SARTO", "rafaela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(0, 9, new Date(), ContractTypeEnum.BASIC, 50.50), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addContractNoCustomerId()throws Exception{
		Customer customer = new Customer(10, "RAFAELA SARTO", "rafaela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 0, new Date(), ContractTypeEnum.BASIC, 50.50), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addContractNoRevenue()throws Exception{
		Customer customer = new Customer(11, "RAFAELA SARTO", "rafaela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 11, new Date(), ContractTypeEnum.BASIC, 0), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addContractNoStartDate()throws Exception{
		Customer customer = new Customer(12, "RAFAELA SARTO", "rafaela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 12, null, ContractTypeEnum.BASIC, 526.96), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void addContractNoType()throws Exception{
		Customer customer = new Customer(13, "RAFAELA SARTO", "rafaela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 13, new Date(), null, 526.96), HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void getCustomer()throws Exception{
		Customer customer = new Customer(14, "DANIELA SARTO", "daniela@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.getCustomer(14, HttpStatus.OK);
	}
	
	@Test
	public void getCustomerNotFound()throws Exception{
		this.getCustomer(10001, HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void getCustomerNoId()throws Exception{
		this.getCustomer(0, HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void sumCustomerRevenue() throws Exception{
		Customer customer = new Customer(15, "MARIA SARTO", "maria@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 15, new Date(), ContractTypeEnum.BASIC, 1000.50), HttpStatus.OK);
		this.addContract(new Contract(2, 15, new Date(), ContractTypeEnum.COMPACT, 100.50), HttpStatus.OK);
		this.addContract(new Contract(3, 15, new Date(), ContractTypeEnum.FULL, 200.50), HttpStatus.OK);
		this.mvc.perform(get("/contract/id/" + customer.getId()))
			.andExpect(status().isOk());
	}
	
	@Test
	public void sumGeneralRevenue() throws Exception{
		Customer customer = new Customer(16, "ANA SARTO", "ana@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 16, new Date(), ContractTypeEnum.BASIC, 1000.50), HttpStatus.OK);
		this.addContract(new Contract(2, 16, new Date(), ContractTypeEnum.COMPACT, 100.50), HttpStatus.OK);
		this.addContract(new Contract(3, 16, new Date(), ContractTypeEnum.FULL, 200.50), HttpStatus.OK);
		
		customer = new Customer(17, "GIACOMO ZATTI", "giacomo@test.com", null);
		this.addCustomer(customer, HttpStatus.OK);
		this.getCustomer(customer.getId(), HttpStatus.OK);
		this.addContract(new Contract(1, 17, new Date(), ContractTypeEnum.BASIC, 1000.50), HttpStatus.OK);
		this.addContract(new Contract(2, 17, new Date(), ContractTypeEnum.COMPACT, 100.50), HttpStatus.OK);
		this.addContract(new Contract(3, 17, new Date(), ContractTypeEnum.FULL, 200.50), HttpStatus.OK);
		
		this.mvc.perform(get("/contract/type/" + ContractTypeEnum.BASIC)).andExpect(status().isOk());		
		this.mvc.perform(get("/contract/type/" + ContractTypeEnum.COMPACT)).andExpect(status().isOk());		
		this.mvc.perform(get("/contract/type/" + ContractTypeEnum.FULL)).andExpect(status().isOk());		
	}
	
	private void addCustomer(Customer customer, HttpStatus expectedStatus) throws Exception{
		this.mvc.perform(put("/customer/" + customer.getId())
							.contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(customer)))
						 .andExpect(expectedStatus == HttpStatus.OK ? status().isOk() : status().isBadRequest());
	}
	
	private void getCustomer(int customerId, HttpStatus expectedStatus)throws Exception{
		this.mvc.perform(get("/customer/" + customerId)).andExpect(expectedStatus == HttpStatus.OK ? status().isOk() : status().isNotFound());
	}
	
	private void addContract(Contract contract, HttpStatus expectedStatus) throws Exception{
		this.mvc.perform(put("/contract/" + contract.getId())
							.contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(contract)))
						 .andExpect(expectedStatus == HttpStatus.OK ? status().isOk() : status().isBadRequest());
	}
	
	private static String toJson(Object object){
		String json = gson.toJson(object);
		System.out.println(json);
		return json;
	}
}
